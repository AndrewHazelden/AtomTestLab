_VERSION = [[Version 1 - 2021-12-24]]
--[[
==============================================================================
Add AtomTestLab Repo.lua - v1 2021-12-24
==============================================================================
**Use at your own risk!**

The AtomTestLab Repo is an R&D laboratory for Reactor atom package creation.

AtomTestLab is also the home of the Kartaverse/KartaVR v5 early access program.

--]]--

-- Reactor Public ProjectID
local reactor_project_id = "5058837"

------------------------------------------------------------------------------
-- Reactor:/System/Reactor.cfg file:
g_ReactorTestLabConfig =
{
	Repos =
	{
		_Core =
		{
			Protocol = "GitLab",
			ID = reactor_project_id,
		},
		Reactor =
		{
			Protocol = "GitLab",
			ID = reactor_project_id,
		},
		AtomTestLab = {
			Protocol = "GitLab",
			ID = "31308292",
		},
	},
	Settings =
	{
		Reactor =
		{
			AskForInstallScriptPermissions = false,
			ConcurrentTransfers = 4,
			LiveSearch = true,
			MarkAsNew = true,
			NewForDays = 7,
			PrevSyncTime = os.time(),
			ViewLayout = "Larger Description View",
		},
	},
}

g_DefaultConfig =
{
	Repos =
	{
		_Core =
		{
			Protocol = "GitLab",
			ID = reactor_project_id,
		},
		Reactor =
		{
			Protocol = "GitLab",
			ID = reactor_project_id,
		},
	},
	Settings =
	{
		Reactor =
		{
			AskForInstallScriptPermissions = true,
			LiveSearch = true,
			MarkAsNew = true,
			NewForDays = 7,
			PrevSyncTime = os.time(),
			ViewLayout = "Balanced View",
		},
	},
}



------------------------------------------------------------------------
-- Return a string with the directory path where the Lua script was run from
-- If the script is run by pasting it directly into the Fusion Console define a fallback path
-- fileTable = GetScriptDir('Reactor:/System/UI/Atomizer.lua')
function GetScriptDir(fallback)
	if debug.getinfo(1).source == '???' then
		-- Fallback absolute filepath
		-- return bmd.parseFilename(app:MapPath(fallback))
		return parseFilename(app:MapPath(fallback))
	else
		-- Filepath coming from the Lua script's location on disk
		-- return bmd.parseFilename(app:MapPath(string.sub(debug.getinfo(1).source, 2)))
		return parseFilename(app:MapPath(string.sub(debug.getinfo(1).source, 2)))
	end
end

------------------------------------------------------------------------------
-- Added emoticon support for local images like <img src="Emoticons:/wink.png">
-- Example: dump(EmoticonParse([[<img src="Emoticons:/wink.png">]]))
-- Added image loading support for local images like <img src="Reactor:/Deploy/Docs/ReactorDocs/Images/atomizer-welcome.png">
function EmoticonParse(str)
	local htmlstr = ''
	htmlstr = string.gsub(str, '[Ee]moticons:/', emoticonsDir)
	htmlstr = string.gsub(htmlstr, "[Rr]eactor:/", reactorDir)
	
	return htmlstr
end

------------------------------------------------------------------------------
-- parseFilename() from bmd.scriptlib
--
-- this is a great function for ripping a filepath into little bits
-- returns a table with the following
--
-- FullPath : The raw, original path sent to the function
-- Path : The path, without filename
-- FullName : The name of the clip w\ extension
-- Name : The name without extension
-- CleanName: The name of the clip, without extension or sequence
-- SNum : The original sequence string, or "" if no sequence
-- Number : The sequence as a numeric value, or nil if no sequence
-- Extension: The raw extension of the clip
-- Padding : Amount of padding in the sequence, or nil if no sequence
-- UNC : A true or false value indicating whether the path is a UNC path or not
------------------------------------------------------------------------------
function parseFilename(filename)
	local seq = {}
	seq.FullPath = filename
	string.gsub(seq.FullPath, "^(.+[/\\])(.+)", function(path, name) seq.Path = path seq.FullName = name end)
	string.gsub(seq.FullName, "^(.+)(%..+)$", function(name, ext) seq.Name = name seq.Extension = ext end)

	if not seq.Name then -- no extension?
		seq.Name = seq.FullName
	end

	string.gsub(seq.Name, "^(.-)(%d+)$", function(name, SNum) seq.CleanName = name seq.SNum = SNum end)

	if seq.SNum then
		seq.Number = tonumber( seq.SNum )
		seq.Padding = string.len( seq.SNum )
	else
		seq.SNum = ""
		seq.CleanName = seq.Name
	end

	if seq.Extension == nil then seq.Extension = "" end
	seq.UNC = ( string.sub(seq.Path, 1, 2) == [[\\]] )

	return seq
end


------------------------------------------------------------------------
-- Convert Unicode characters into HTML entities
-- Example: EncodeHTML('¿')
function EncodeHTML(txt)
	if txt ~= nil then
		htmlCharacters = {
			{pattern = '¡', replace = '&iexcl;'},
			{pattern = '¿', replace = '&iquest;'},
			{pattern = '·', replace = '&middot;'},
			{pattern = '«', replace = '&laquo;'},
			{pattern = '»', replace = '&raquo;'},
			{pattern = '〈', replace = '&#x3008;'},
			{pattern = '〉', replace = '&#x3009;'},
			{pattern = '§', replace = '&sect;'},
			{pattern = '¶', replace = '&para;'},
			{pattern = '%[', replace = '&#91;'},
			{pattern = '%]', replace = '&#93;'},
			{pattern = '‰', replace = '&permil;'},
			{pattern = '†', replace = '&dagger;'},
			{pattern = '‡', replace = '&Dagger;'},
			{pattern = '¨', replace = '&uml;'},
			{pattern = '°', replace = '&deg;'},
			{pattern = '©', replace = '&copy;'},
			{pattern = '®', replace = '&reg;'},
			{pattern = '∇', replace = '&nabla;'},
			{pattern = '∈', replace = '&isin;'},
			{pattern = '∉', replace = '&notin;'},
			{pattern = '∋', replace = '&ni;'},
			{pattern = '±', replace = '&plusmn;'},
			{pattern = '÷', replace = '&divide;'},
			{pattern = '×', replace = '&times;'},
			{pattern = '≠', replace = '&ne;'},
			{pattern = '¬', replace = '&not;'},
			{pattern = '√', replace = '&radic;'},
			{pattern = '∞', replace = '&infin;'},
			{pattern = '∠', replace = '&ang;'},
			{pattern = '∧', replace = '&and;'},
			{pattern = '∨', replace = '&or;'},
			{pattern = '∩', replace = '&cap;'},
			{pattern = '∪', replace = '&cup;'},
			{pattern = '∫', replace = '&int;'},
			{pattern = '∴', replace = '&there4;'},
			{pattern = '≅', replace = '&cong;'},
			{pattern = '≈', replace = '&asymp;'},
			{pattern = '≡', replace = '&equiv;'},
			{pattern = '≤', replace = '&le;'},
			{pattern = '≥', replace = '&ge;'},
			{pattern = '⊂', replace = '&sub;'},
			{pattern = '⊄', replace = '&nsub;'},
			{pattern = '⊃', replace = '&sup;'},
			{pattern = '⊆', replace = '&sube;'},
			{pattern = '⊇', replace = '&supe;'},
			{pattern = '⊕', replace = '&oplus;'},
			{pattern = '⊗', replace = '&otimes;'},
			{pattern = '⊥', replace = '&perp;'},
			{pattern = '◊', replace = '&loz; '},
			{pattern = '♠', replace = '&spades;'},
			{pattern = '♣', replace = '&clubs;'},
			{pattern = '♥', replace = '&hearts;'},
			{pattern = '♦', replace = '&diams;'},
			{pattern = '¤', replace = '&curren;'},
			{pattern = '¢', replace = '&cent;'},
			{pattern = '£', replace = '&pound;'},
			{pattern = '¥', replace = '&yen;'},
			{pattern = '€', replace = '&euro;'},
			{pattern = '¹', replace = '&sup1;'},
			{pattern = '½', replace = '&frac12;'},
			{pattern = '¼', replace = '&frac14;'},
			{pattern = '²', replace = '&sup2;'},
			{pattern = '³', replace = '&sup3;'},
			{pattern = '¾', replace = '&frac34;'},
			{pattern = 'ª', replace = '&ordf;'},
			{pattern = 'ƒ', replace = '&fnof;'},
			{pattern = '™', replace = '&trade;'},
			{pattern = 'β', replace = '&beta;'},
			{pattern = 'Δ', replace = '&Delta;'},
			{pattern = 'ϑ', replace = '&thetasym;'},
			{pattern = 'Θ', replace = '&Theta;'},
			{pattern = 'ι', replace = '&iota;'},
			{pattern = 'λ', replace = '&lambda;'},
			{pattern = 'Λ', replace = '&Lambda;'},
			{pattern = 'μ', replace = '&mu;'},
			{pattern = 'µ', replace = '&micro;'},
			{pattern = 'ξ', replace = '&xi;'},
			{pattern = 'Ξ', replace = '&Xi;'},
			{pattern = 'π', replace = '&pi;'},
			{pattern = 'ϖ', replace = '&piv;'},
			{pattern = 'Π', replace = '&Pi;'},
			{pattern = 'ρ', replace = '&rho;'},
			{pattern = 'σ', replace = '&sigma;'},
			{pattern = 'ς', replace = '&sigmaf;'},
			{pattern = 'Σ', replace = '&Sigma;'},
			{pattern = 'τ', replace = '&tau;'},
			{pattern = 'υ', replace = '&upsilon;'},
			{pattern = 'ϒ', replace = '&upsih;'},
			{pattern = 'φ', replace = '&phi;'},
			{pattern = 'Φ', replace = '&Phi;'},
			{pattern = 'χ', replace = '&chi;'},
			{pattern = 'ψ', replace = '&psi;'},
			{pattern = 'Ψ', replace = '&Psi;'},
			{pattern = 'ω', replace = '&omega;'},
			{pattern = 'Ω', replace = '&Omega;'},
		}

		for i,val in ipairs(htmlCharacters) do
			txt = string.gsub(txt, htmlCharacters[i].pattern, htmlCharacters[i].replace)
		end
	end

	return txt
end

-- AskOK Confirmation Dialog
-- Example: add, remove = AskOK('The Window Title', 'Write your message here')
function AskOK(title, description)
	local ok = false
	ui = fu.UIManager
	disp = bmd.UIDispatcher(ui)

	------------------------------------------------------------------------
	-- Add the platform specific folder slash character
	osSeparator = package.config:sub(1,1)

	-- print the AskOK dialog message to the console
	print('[' .. tostring(title) .. '] ' .. tostring(description))

	------------------------------------------------------------------------
	-- Find the Reactor icon images
	iconsDir = fusion:MapPath('Reactor:/System/UI/Images/') .. 'icons.zip' .. osSeparator
	large = 32
	iconsMedium = {large,large}

	------------------------------------------------------------------------
	-- Create the new window
	local okwin = disp:AddWindow({
		ID = 'okwin',
		TargetID = 'okwin',
		WindowTitle = title,
		Geometry = {200,100,380,165},
		MinimumSize = {540, 215},
		-- Spacing = 10,
		-- Margin = 20,

		ui:VGroup{
			ID = 'root',

			-- Atom Working Directory
			ui:HGroup{
				ui:TextEdit{
					ID = 'DescriptionLabel',
					Weight = 1,
					ReadOnly = true,
					Text = description,
				},
			},

			ui:VGap(5, 0),

			ui:HGroup{
				Weight = 0,
				ui:Button{
					ID = 'CancelButton',
					Text = 'Cancel',
					IconSize = iconsMedium,
					Icon = ui:Icon{
						File = iconsDir .. 'close.png'
					},
					MinimumSize = iconsMedium,
					Flat = true,
				},

				ui:HGap(65),

				ui:Button{
					ID = 'RemoveButton',
					Text = 'Remove Repo',
					IconSize = iconsMedium,
					Icon = ui:Icon{
						File = iconsDir .. 'quit.png'
					},
					MinimumSize = iconsMedium,
					Flat = true,
				},
				
				ui:HGap(65),

				ui:Button{
					ID = 'AddButton',
					Text = 'Add Repo',
					IconSize = iconsMedium,
					Icon = ui:Icon{
						File = iconsDir .. 'create.png'
					},
					MinimumSize = iconsMedium,
					Flat = true,
				},
			},
		}
	})

	-- The window was closed
	function okwin.On.okwin.Close(ev)
		add = false
		disp:ExitLoop()
	end

	function okwin.On.CancelButton.Clicked(ev)
		add = false
		remove = false
		disp:ExitLoop()
	end

	function okwin.On.RemoveButton.Clicked(ev)
		add = false
		remove = true
		disp:ExitLoop()
	end

	function okwin.On.AddButton.Clicked(ev)
		add = true
		remove = false
		disp:ExitLoop()
	end

	app:AddConfig('okwin', {
		Target{ID = 'okwin'},
		Hotkeys{
			Target = 'okwin',
			Defaults = true,

			CONTROL_W = 'Execute{cmd = [[ app.UIManager:QueueEvent(obj, "Close", {}) ]]}',
			CONTROL_F4 = 'Execute{cmd = [[ app.UIManager:QueueEvent(obj, "Close", {}) ]]}',
		},
	})

	okwin:Show()
	disp:RunLoop()
	okwin:Hide()
	app:RemoveConfig('okwin')

	-- Report the status
	-- print('[' .. tostring(title) .. '] ' .. tostring(add))

	return add, remove, okwin,okwin:GetItems()
end


------------------------------------------------------------------------------
-- The Main function - Where the magic happens!
function Main()
	msg = EmoticonParse([[<p>The AtomTestLab Repo is an R&D laboratory for Reactor atom package creation. AtomTestLab is also the home of the Kartaverse/KartaVR v5 early access program. Known side-effects from using the AtomTestLab Repo include feelings of:<br>

<img src="Emoticons:/banana.png">
<img src="Emoticons:/banghead.png">
<img src="Emoticons:/biggrin.png">
<img src="Emoticons:/bowdown.png">
<img src="Emoticons:/buttrock.png">
<img src="Emoticons:/cheer.png">
<img src="Emoticons:/cheers.png">
<img src="Emoticons:/confused.png">
<img src="Emoticons:/cool.png">
<img src="Emoticons:/cry.png">
<img src="Emoticons:/eek.png">
<img src="Emoticons:/evil.png">
<img src="Emoticons:/exclaim.png">
<img src="Emoticons:/facepalm.png">
<img src="Emoticons:/geek.png">
<img src="Emoticons:/idea.png">
<img src="Emoticons:/lol.png">
<img src="Emoticons:/mad.png">
<img src="Emoticons:/mrgreen.png">
<img src="Emoticons:/neutral.png">
<img src="Emoticons:/nocheer.png">
<img src="Emoticons:/popcorn.png">
<img src="Emoticons:/question.png">
<img src="Emoticons:/razz.png">
<img src="Emoticons:/redface.png">
<img src="Emoticons:/rolleyes.png">
<img src="Emoticons:/sad.png">
<img src="Emoticons:/smile.png">
<img src="Emoticons:/surprised.png">
<img src="Emoticons:/twisted.png">
<img src="Emoticons:/ugeek.png">
<img src="Emoticons:/whistle.png">
<img src="Emoticons:/wink.png">
<img src="Emoticons:/wip.png">
<img src="Emoticons:/arrow.png"></p>]])


	-- Show a confirmation dialog
	addRepo, removeRepo = AskOK('Add AtomTestLab Repo', msg)
	if addRepo == true then
		-- Adding AtomTestLab Repo
		print('[Editing Reactor.cfg] Started Adding AtomTestLab Repo')

		-- Add the platform specific folder slash character
		local osSeparator = package.config:sub(1,1)

		-- Check for a pre-existing PathMap preference
		local reactor_existing_pathmap = app:GetPrefs("Global.Paths.Map.Reactor:")
		if reactor_existing_pathmap and reactor_existing_pathmap ~= "nil" then
			-- Clip off the "reactor_root" style trailing "Reactor/" subfolder
			reactor_existing_pathmap = string.gsub(reactor_existing_pathmap, "Reactor" .. osSeparator .. "$", "")
		end

		local reactor_pathmap = os.getenv("REACTOR_INSTALL_PATHMAP") or reactor_existing_pathmap or "AllData:"
		local reactor_root = app:MapPath(tostring(reactor_pathmap) .. "Reactor/")

		-- Save the updated repo table into Reactor.cfg
		bmd.writefile(reactor_root .. "System/Reactor.cfg", g_ReactorTestLabConfig)

		print("[Active Reactor Repos]")
		-- Read back in the Reactor.cfg file
		local g_CurrentConfig = bmd.readfile(reactor_root .. "System/Reactor.cfg")
		dump(g_CurrentConfig.Repos)

		-- Display the Reactor Window to auto refresh the atom files
		local reactorScript = app:MapPath(tostring(reactor_pathmap) .. 'Reactor/System/') .. 'Reactor.lua'
		if bmd.fileexists(reactorScript) == false then
			print('\n\n[Reactor Error] Open the Reactor window once to download the missing file: ' .. reactorScript)
		else
			print('\n\n[Reactor] Opening Reactor Window')
			comp:RunScript(reactorScript)
		end
	elseif removeRepo == true then
		-- Removing ReactorTestLab Repo
		print('[Editing Reactor.cfg] Started Removing AtomTestLab Repo')

		-- Add the platform specific folder slash character
		local osSeparator = package.config:sub(1,1)

		-- Check for a pre-existing PathMap preference
		local reactor_existing_pathmap = app:GetPrefs("Global.Paths.Map.Reactor:")
		if reactor_existing_pathmap and reactor_existing_pathmap ~= "nil" then
			-- Clip off the "reactor_root" style trailing "Reactor/" subfolder
			reactor_existing_pathmap = string.gsub(reactor_existing_pathmap, "Reactor" .. osSeparator .. "$", "")
		end

		local reactor_pathmap = os.getenv("REACTOR_INSTALL_PATHMAP") or reactor_existing_pathmap or "AllData:"
		local reactor_root = app:MapPath(tostring(reactor_pathmap) .. "Reactor/")

		-- Save the updated repo table into Reactor.cfg
		bmd.writefile(reactor_root .. "System/Reactor.cfg", g_DefaultConfig)

		print("[Active Reactor Repos]")
		-- Read back in the Reactor.cfg file
		local g_CurrentConfig = bmd.readfile(reactor_root .. "System/Reactor.cfg")
		dump(g_CurrentConfig.Repos)

		-- Display the Reactor Window to auto refresh the atom files
		local reactorScript = app:MapPath(tostring(reactor_pathmap) .. 'Reactor/System/') .. 'Reactor.lua'
		if bmd.fileexists(reactorScript) == false then
			print('\n\n[Reactor Error] Open the Reactor window once to download the missing file: ' .. reactorScript)
		else
			print('\n\n[Reactor] Opening Reactor Window')
			comp:RunScript(reactorScript)
		end
	else
		print('[Add ReactorTestLab Repo] You cancelled the dialog!')

		-- Exit the script
		return
	end
end

------------------------------------------------------------------------
-- Find out the current operating system platform. The platform variable should be set to either 'Windows', 'Mac', or 'Linux'.
platform = (FuPLATFORM_WINDOWS and 'Windows') or (FuPLATFORM_MAC and 'Mac') or (FuPLATFORM_LINUX and 'Linux')

------------------------------------------------------------------------
-- Add the platform specific folder slash character
osSeparator = package.config:sub(1,1)

------------------------------------------------------------------------------
-- Reactor Deploy Folder
reactorDir = app:MapPath('Reactor:/')
deployDir = app:MapPath('Reactor:/Deploy')

------------------------------------------------------------------------------
-- Load the emoticons as standalone PNG image resources
emoticonsDir = fusion:MapPath('Reactor:/System/UI/Emoticons') .. osSeparator
-- Load the Atomizer script icons as from a single ZIPIO bundled resource
iconsDir = fusion:MapPath('Reactor:/System/UI/Images') .. osSeparator .. 'icons.zip' .. osSeparator

Main()
print('[Done]')
