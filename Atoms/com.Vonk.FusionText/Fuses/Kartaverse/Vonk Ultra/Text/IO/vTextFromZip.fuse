-- ============================================================================
-- modules
-- ============================================================================
local jsonutils = self and require("vjsonutils") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vTextFromZip"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Text\\IO",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Reads a Text string from a zip archive.",
    REGS_OpIconString = FUSE_NAME
})

function Create()
    -- [[ Creates the user interface. ]]
    InZipFile = self:AddInput("Zip File" , "ZipFile" , {
        LINKID_DataType = "Text",
        INPID_InputControl = "FileControl",
        FC_IsSaver = false,
        FC_ClipBrowse = false,
        FCS_FilterString =  "Zip (*.zip)|*.zip",
        LINK_Main = 1
    })

    InExtractFile = self:AddInput("Extract File" , "ExtractFile" , {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        TEC_Lines = 1,
        LINK_Main = 2
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutText = self:AddOutput("Output" , "Output" , {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })
end


function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InZipFile:SetAttrs({LINK_Visible = visible})
        InExtractFile:SetAttrs({LINK_Visible = visible})
    end
end


-- Unzip a file and return it as a string filled with binary data
function UnzipReadFile(zipFilename, extractFilename)
    if zipFilename == "" then
        error('[Zip] [Zip File] Empty filepath string')
    elseif extractFilename == "" then
        error('[Zip] [Extract File] Empty filepath string')
    end

    -- Create a file handler for the fuskin zipfile
    local zip = ZipFile(zipFilename, false)

    -- Verify the zip file could be accessed
    if zip:IsOpen() then
        -- print('[Zip] [Opened Zip] ', zipFilename)

        -- Search for a file
        if zip:LocateFile(extractFilename, true) then
            -- print('[Zip] [Found] ', extractFilename)

            -- Access the specific image
            if zip:OpenFile() then
                -- print('[Zip]\t[Opened File] ', tostring(zip:GetFileName()))

                -- Print the file creation date as YYYY-MM-DD
                -- print('[Zip]\t[File Date] ', os.date('%Y-%m-%d', zip:GetFileTime()))

                filesize = tonumber(zip:GetFileSize())
                -- print('[Zip]\t[File Size] ', tostring(filesize))

                -- Create an FFI based buffer variable to hold the extracted file data
                -- Note: You might have to increase the 'BUF_SIZE' value to allow larger then 65K files to be extracted in your scripts.
                local BUF_SIZE = 65536
                local buf = ffi.new('char[?]', BUF_SIZE)

                -- Read the file from the zip
                -- The 'zip:readfile()' size argument should be set to the value returned by 'zip:GetFileSize()'
                local len = zip:ReadFile(buf, filesize)
                -- print('[Zip]\t[Bytes Read] ', tostring(len))

                -- Close the zip
                zip:CloseFile()

                if len > 0 then
                    -- print('[Zip]\t[Extracted File Sucessfully] ')

                    -- return the binary data that was extracted
                    return ffi.string(buf, len)
                else
                    error(string.format('[Zip]\t[Extracted File is Empty] '))
                    return nil
                end
            else
                error(string.format('[Zip]\t[Unable to Open File] %s', extractFilename))
                return nil
            end
        else
            error(string.format('[Zip]\t[Not Found] %s', extractFilename))
            return nil
        end
    else
        error(string.format('[Zip] [Unable to Open Zip] %s', zipFilename))
        return nil
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local rel_path = InZipFile:GetValue(req).Value
    local abs_path = self.Comp:MapPath(rel_path)

    local extract = InExtractFile:GetValue(req).Value

    local txt_str = UnzipReadFile(abs_path, extract)
    local out = Text(txt_str)

    OutText:Set(req, out)
end
