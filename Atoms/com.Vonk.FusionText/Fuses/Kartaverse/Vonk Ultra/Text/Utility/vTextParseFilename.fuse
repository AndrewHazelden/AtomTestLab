-- ============================================================================
-- modules
-- ============================================================================
local textutils = self and require("vtextutils") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vTextParseFilename"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Text\\Utility",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Creates a Fusion Text object by parsing a filepath.",
    REGS_OpIconString = FUSE_NAME
})

function Create()
    -- [[ Creates the user interface. ]]
    InText = self:AddInput("Text" , "Text" , {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        TEC_Lines = 1,
        LINK_Main = 1,
    })

    InParse = self:AddInput("Parse", "Parse", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ComboControl",
        INP_Default = 0,
        INP_Integer = true,
        ICD_Width = 1,
        {CCS_AddString = "FullPath"},
        {CCS_AddString = "FullPathMap"},
        {CCS_AddString = "Path"},
        {CCS_AddString = "PathMap"},
        {CCS_AddString = "FullName"},
        {CCS_AddString = "Name"},
        {CCS_AddString = "CleanName"},
        {CCS_AddString = "SNum"},
        {CCS_AddString = "Number"},
        {CCS_AddString = "Extension"},
        {CCS_AddString = "Padding"},
        {CCS_AddString = "UNC"},
        {CCS_AddString = "Path + Name"},
        CC_LabelPosition = "Vertical"
    })

--    InViewFile = self:AddInput('View File', 'View File', {
--        LINKID_DataType = 'Number',
--        INPID_InputControl = 'ButtonControl',
--        INP_DoNotifyChanged = true,
--        INP_External = false,
--        ICD_Width = 1,
--        INP_Passive = true,
--        IC_Visible = true,
--        BTNCS_Execute = [[
---- check if a tool is selected
--local selectedNode = tool or comp.ActiveTool
--if selectedNode then
--    local filename = selectedNode:GetInput('Text')
--    if filename then
--        if bmd.fileexists(comp:MapPath(filename)) then
--            bmd.openfileexternal('Open', comp:MapPath(filename))
--        else
--            print('[View File] File does not exist yet:', filename)
--        end
--    else
--        print('[View File] Filename is nil. Possibly the text is sourced from an external text input connection.')
--    end
--else
--    print('[View File] Please select the node.')
--end
--        ]],
--    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutText = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InText:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local str = InText:GetValue(req).Value

    -- Check the filename string is not empty
    if str ~= "" then
        local tbl = textutils.parse_filename(str)

        local parse_type = InParse:GetValue(req).Value

        local out = ""
        if parse_type == 0 then
            -- FullPath
            out = tbl.FullPath
        elseif parse_type == 1 then
            -- FullPathMap
            out = tbl.FullPathMap
        elseif parse_type == 2 then
            -- Path
            out = tbl.Path
        elseif parse_type == 3 then
            -- PathMap
            out = tbl.PathMap
        elseif parse_type == 4 then
            -- FullName
            out = tbl.FullName
        elseif parse_type == 5 then
            -- Name
            out = tbl.Name
        elseif parse_type == 6 then
            -- CleanName
            out = tbl.CleanName
        elseif parse_type == 7 then
            -- SNum
            out = tbl.SNum
        elseif parse_type == 8 then
            -- Number
            out = tbl.Number
        elseif parse_type == 9 then
            -- Extension
            out = tbl.Extension
        elseif parse_type == 10 then
            -- Padding
            out = tbl.Padding or 0
        elseif parse_type == 11 then
            -- UNC
            if tbl.UNC ~= nil and tbl.UNC == true then
                out = 1
            else
                out = 0
            end
        elseif parse_type == 12 then
            -- Path + Name
            out = tostring(tbl.Path or "") .. tostring(tbl.Name or "")
        end

    --    dump("FullPath:", tbl.FullPath)
    --    dump("FullPathMap:", tbl.FullPathMap)
    --    dump("Path:", tbl.Path)
    --    dump("PathMap:", tbl.PathMap)
    --    dump("FullName:", tbl.FullName)
    --    dump("Name:", tbl.Name)
    --    dump("CleanName:", tbl.CleanName)
    --    dump("SNum:", tbl.SNum)
    --    dump("Number:", tbl.Number)
    --    dump("Extension:", tbl.Extension)
    --    dump("Padding:", tbl.Padding)
    --    dump("UNC:", tbl.UNC)
    --    dump("Path + Name:", tostring(tbl.Path or "") .. tostring(tbl.Name or ""))
    
        OutText:Set(req, Text(out or ""))
    else
        OutText:Set(req, Text(""))
    end

end
