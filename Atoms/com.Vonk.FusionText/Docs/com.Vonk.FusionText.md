# Vonk Ultra - Text

Text is a node based string library for Blackmagic Design Fusion.

## Acknowledgements

- Kristof Indeherberge
- Cédric Duriau

## Contents

**Fuses**

- `vTextCreate.fuse`: Fuse to create a Fusion Text object.
- `vTextSubJoin.fuse`: Fuse to join strings together into one.
- `vTextSubFormat.fuse`: Fuse to format strings in a template string.
- `vTextSubLength.fuse`: Fuse to return the length of a string.
- `vTextSubLStrip.fuse`: Fuse to remove a leading substring of a string.
- `vTextSubReplace.fuse`: Fuse to replace a substring of a string.
- `vTextSubRStrip.fuse`: Fuse to remove a trailing substring of a string.
- `vTextSubSub.fuse`: Fuse to return a substring of a string.
- `vTextTimeSpeed.fuse`: Fuse to execute time based operation on text.
- `vTextTimeStretch.fuse`: Fuse to execute time based operations on text.


**Modules/Lua**

- `textutils.lua`: Core module for string operations.
