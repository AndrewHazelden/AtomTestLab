# Vonk Ultra

2022-04-20

Vonk Ultra is a collection of data nodes for Blackmagic Design Resolve/Fusion. Vonk can be thought of as node-based modifiers that live in the flow. These node-based operations provide a no-code alternative to using expressions or custom scripts. Data nodes are tools that allow you to interconnect nodes together by supporting more data types for the input and output connections such as numbers, text, spreadsheets, JSON, metadata, arrays, matrices, and more.

These data node-based techniques encourage a more seamless interchange of information between DCC tools by reducing the loss of important metadata, removing manual data entry steps that can be error-prone, and to keep data flowing through a pipeline more organized and consistent.

The long-term hope of Vonk's developers is to help encourage artists and TDs to adopt "data node" concepts across a full production pipeline. These approaches are beneficial for teams working on cutting-edge projects in the VFX, XR, computer vision, machine learning, video/photogrammetry, and digital production/VP space.

## Open-Source License
GPL V3

## Acknowledgements

The original Spicy Acorn Vonk toolset was created by:

- Kristof Indeherberge
- Cédric Duriau

The Vonk Ultra fork is maintained by:

- Andrew Hazelden

## Vonk Usage

The Vonk nodes can be found in Fusion Studio's "Tools > Vonk Ultra" menu. The Vonk nodes all have an initial "v" prefix which makes it easier to browse in the Fusion "Select Tool" dialog which is accessed using the Shift + Spacebar hotkey.

Vonk tools allow you to interconnect mathematical operator nodes (called modifiers) that work together in the flow to visually build formulas that live-update on each frame of a composite. You can also connect several text based Vonk nodes together to edit textual strings on the fly or read/write data like JSON or Text files.

Besides connecting Vonk nodes of together, you can also right-click on a Number or Text based attribute in the Inspector window on a regular Fusion based node, and use the "Connect To" contextual menu to link to a Vonk node added to the node graph, like "vNumberCreate" or "vTextCreate", or many other types of Vonk node based operators.

## Example Comps

The "Reactor:/Deploy/Comps/Vonk Ultra" folder is where the WIP programer-art like example composites for Vonk can be found. Start by exploring the "Demo Number.comp", "Demo Text.comp", and "Demo JSON.comp" examples to see how various data types can be passed as input and output connections between the nodes. 


## Fusion Render Node Customization

We need to add a custom "LuaModules:" PathMap entry in the Fusion Render Node app to avoid Fusion Render Manager errors when batch rendering comps that use Vonk data nodes.

1. Open the Fusion Render Node program and select the "Preferences..." menu item.

2. Click on the "Global Settings > PathMap" category on the left side of the Preferences window.

3. At the bottom of the window click on the "New" button to add a new entry to the "User" section of the Path Map view.

4. Enter the following settings `From: LuaModules:` and `To: UserPaths:Modules/Lua`. Click the "Save" button to retain these settings.

5. If you don't have Reactor Path Map entries added to Fusion Render Node already, then you might have to further customize the preferences to add values like:

    **Windows:**

    `From: Reactor:` and `To: C:\ProgramData\Blackmagic Design\Fusion\Reactor\`

    **macOS:**

    `From: Reactor:` and `To: /Library/Application Support/Blackmagic Design/Fusion/Fusion/Reactor/`

    **Linux:**

    `From: Reactor:` and `To: /var/BlackmagicDesign/Fusion/Reactor/`


    Also you would need to edit the pre-existing UserPaths PathMap entry:

    `From: UserPaths:` and `To: UserData:;AllData:;Fusion:;Reactor:Deploy`

6. Restart Fusion Render Manager to lock in these values.

Note: If the LuaModules PathMap entries were not added to Fusion Render Node's preferences, a typical error message in Fusion Render Node would look a bit like this:

        13/Apr/22 16:12:50: .../Fusion/Fuses/Vonk Ultra/Text/Create/vTextFromArray.fuse:5: module 'vjsonutils' not found:
            no field package.preload['vjsonutils']
            no file 'LuaModules:vjsonutils.lua'
            no file 'LuaModules:vjsonutils/init.lua'
            no file 'LuaModules:vjsonutils.so'

