Atom {
	Name = "Lua Matrix",
	Category = "Modules/Lua",
	Author = "Michael Lutz",
	Version = 1,
	Date = {2022, 3, 1},
	Description = [[<p>Matrices and matrix operations implemented in pure Lua.</p>

<p>This supports operations on matrices and vectors whose elements are
real, complex, or symbolic.  Implemented entirely in Lua as tables.
Includes a complex number data type too.</p>

<p>Open-Source License<br>
Lua Matrix is licensed using the MIT license.</p>

<p>Acknowledgements<br>
By Michael Lutz (original author), David Manura (maintainer).<br>
Bug fixes/comments: Geoff Richards, SatheeshJM, Martin Ossmann, and others.</p>

]],
	Deploy = {
		"Docs/com.MichaelLutz.LuaMatrix.txt",
		"Modules/Lua/complex.lua",
		"Modules/Lua/matrix.lua",
	},
}
