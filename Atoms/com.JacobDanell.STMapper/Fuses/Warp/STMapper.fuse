--[[--
Made by Jacob Danell <jacob@emberlight.se>

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
----------------------------------------------------------------------

Todo:
	- Work with RoI

Changelog:
v1.0.2, 2022-02-25:
- Added Repeat Pivot that lets you select where the pivot point of the repeat is located
- Cleaned up the UX by replacing "Calculate STmap from pixel center" with "Match Render" where you can select between the 3 different types of STMaps.
	Ignore = First pixel is 0, last pixel is 1.
	Image = This is what Custom Node-generated STmaps looks like and what the Texture node expects.
	Mesh = This is what Nuke-generated STmaps looks like.
- Removed Crop to DoD that you could set when using Tile or Mirror (honestly I don't remember what it was there for...)
- Rewrote the Tile and Mirror tiling so it no longer gives weird artifacts when the input images DoD is smaller than the image size.

v1.0.1 2022-02-03:
- STMapper now expects the first pixel to be 0 and the pixel next to the last pixel to be 1. This is how Fusions Texture node works.
- Added "Calculate STmap from pixel center" to read STmaps like Nuke does.

v1.0, 2020-10-12:
- First release!

--]]--

FuRegisterClass("STMapper", CT_Tool, {
    REGS_Name			= "STMapper",
    REGS_Category		= "Warp",
    REGS_OpIconString	= "SM",
    REGS_OpDescription	= "STMapper",
	REGS_Company		= "Ember Light",
	REGS_URL			= "",
	REGS_HelpTopic		= "",

	REG_Fuse_NoEdit		= false,
	REG_Fuse_NoReload	= false,
	REG_SupportsDoD		= true,
	REG_Version 		= 102,

	REG_NoAutoProxy			= true,
	REG_NoMotionBlurCtrls	= true,
	REG_NoObjMatCtrls		= true,
	REG_NoBlendCtrls		= true,
	REG_OpNoMask			= true,
    })

MAX_SIZE = 32767	-- images can't get larger than this

function Create()

	InOperation = self:AddInput("Crop", "Crop", {
		LINKID_DataType      = "Number",
		INPID_InputControl   = "MultiButtonControl",
		INP_Default          = 0.0,
		{MBTNC_AddButton     = "Image Frame" },
		{MBTNC_AddButton     = "Image Frame with DoD" },
		{MBTNC_AddButton     = "STMap Frame" },
		MBTNC_StretchToFit   = true,
		MBTNC_ShowName       = true,
		})

	InUChannel = self:AddInput("U Channel", "UCHannel", {
		LINKID_DataType = "Number",
		INPID_InputControl = "MultiButtonControl",
		INP_Default = 0.0,
		{ MBTNC_AddButton = "Red"},        
		{ MBTNC_AddButton = "Green"},
		{ MBTNC_AddButton = "Blue"},
		INP_Integer = true,
		})	

	InVChannel = self:AddInput("V Channel", "VCHannel", {
		LINKID_DataType = "Number",
		INPID_InputControl = "MultiButtonControl",
		INP_Default = 1.0,
		{ MBTNC_AddButton = "Red"},        
		{ MBTNC_AddButton = "Green"},
		{ MBTNC_AddButton = "Blue"},
		INP_Integer = true,
		})

	InFlipU = self:AddInput("Flip U", "FlipU", {
		LINKID_DataType = "Number",
		INPID_InputControl = "CheckboxControl",
		INP_Default = 0.0,
		ICD_Width = 0.5,
		INP_Integer = true,
		})

	InFlipV = self:AddInput("Flip V", "FlipV", {
		LINKID_DataType = "Number",
		INPID_InputControl = "CheckboxControl",
		INP_Default = 0.0,
		ICD_Width = 0.5,
		INP_Integer = true,
		})

	InOffset = self:AddInput("Offset", "Offset", {
		LINKID_DataType = "Point",
		INPID_InputControl = "OffsetControl",
		INPID_PreviewControl = "CrosshairControl",
		})

	InTiling = self:AddInput("Tiling", "Tiling", {
		LINKID_DataType = "Number",
		INPID_InputControl = "MultiButtonControl",
		INP_Default = 0.0,
		{ MBTNC_AddButton = "Transparent"},
		{ MBTNC_AddButton = "Extend"},
		{ MBTNC_AddButton = "Tile"},
		{ MBTNC_AddButton = "Mirror"},
		INP_DoNotifyChanged = true,
		INP_Integer = true,
		})

	InRepeatPivot = self:AddInput("Repeat Pivot", "RepeatPivot", {
		LINKID_DataType = "Point",
		INPID_InputControl = "OffsetControl",
		INPID_PreviewControl = "CrosshairControl",
		})

	InHRepeat = self:AddInput("Horizontal Repeat", "HorizontalRepeat", {
		LINKID_DataType = "Number",
		INPID_InputControl = "ScrewControl",
        INP_Default = 1.0,
		})

	InVRepeat = self:AddInput("Vertical Repeat", "VerticalRepeat", {
		LINKID_DataType = "Number",
		INPID_InputControl = "ScrewControl",
        INP_Default = 1.0,
		})

	InCombRepeat = self:AddInput("Combined Repeat", "CombinedRepeat", {
		LINKID_DataType = "Number",
		INPID_InputControl = "ScrewControl",
        INP_Default = 1.0,
		})

	InMatchRender = self:AddInput("Match Render", "MatchRender", {
		LINKID_DataType = "Number",
		INPID_InputControl = "MultiButtonControl",
		INP_Default = 1.0,
		{ MBTNC_AddButton = "Ignore (0 to Width)", MBTNCS_ToolTip = "First pixel is 0, last pixel is 1"},
		{ MBTNC_AddButton = "Image (0 to Width-1)", MBTNCS_ToolTip = "This is what Custom Node-generated STmaps looks like and what the Texture node expects"},
		{ MBTNC_AddButton = "Mesh (0.5 to Width-0.5)", MBTNCS_ToolTip = "This is what Nuke-generated STmaps looks like"},
		INP_DoNotifyChanged = true,
		INP_Integer = true,
		})

	InPreDivideSTMap = self:AddInput("Pre-Divide STMap by Alpha", "PreDivideSTMapbyAlpha", {
		LINKID_DataType = "Number",
		INPID_InputControl = "CheckboxControl",
		INP_Default = 1.0,
		INP_Integer = true,
		})

	InPostMultiplyRGB = self:AddInput("Post-Multiply RGB by Alpha", "PostMultiplyRGBbyAlpha", {
		LINKID_DataType = "Number",
		INPID_InputControl = "CheckboxControl",
		INP_Default = 1.0,
		INP_Integer = true,
		})
		
	InImage = self:AddInput("Texture", "Texture", {
		LINKID_DataType = "Image",
		LINK_Main = 2,
		INP_AcceptsCLImages = true,
		INP_Priority = -1,
		INP_Required = true,
		})

	InSTMapImage = self:AddInput("Input", "Input", {
		LINKID_DataType = "Image",
		LINK_Main = 1,
		INP_AcceptsCLImages = true,
		INP_Priority = -2,
		INP_Required = true,
		})	
	
    OutImage = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Image",
        LINK_Main = 1,
        })
end

-- request the whole DoD from upstream. Without overriding CheckRequest, Fusion would
-- only request what it requested from this tool (the current RoI, that is)
function CheckRequest(req)
	if (req:GetPri() <= -1) and (not req:IsFailed()) then
		-- input DoD: information about the input image (an ImageDomain instance that also contains information about the image not just the DoD)
		local inpdod
		if (req:GetPri() == -1) then
			inpdod = req:GetInputDoD(InImage)
		else
			inpdod = req:GetInputDoD(InSTMapImage)
		end
		-- request DoD: the output DoD which has been determined during the precalc request 
		local reqdod = req:GetDoD()
		if (inpdod ~= nil) and (reqdod ~= nil) and (inpdod.ValidWindow:Width() <= MAX_SIZE) and (inpdod.ValidWindow:Height() <= MAX_SIZE) then
			-- request RoI: the area that's being requested from this tool
			local reqroi = req:GetRoI()
			if reqroi ~= nil then
				-- in here we're dealing with a real request, not a precalc
				-- data window to be requested from upstream is based on current RoI, limited by our precalculated output DoD
				local datawnd = reqroi

				-- request new datawindow from upstream, limited by the input's DoD (for safety)
				if (req:GetPri() == -1) then
					req:SetInputRoI(InImage, inpdod:Intersect(datawnd))
				else
					req:SetInputRoI(InSTMapImage, inpdod:Intersect(datawnd))
				end
			end -- if reqroi
		end
	end
end


function ClacDod(req)
	local inimg = InImage:GetValue(req)
	local stMapimg = InSTMapImage:GetValue(req)

	local op =  math.floor(InOperation:GetValue(req).Value + 0.5)

	-- attributes for output image (to be extended further down)
	local outattrs, dod, datawnd

	-- calculate output image dimension and output DoD (datawnd)
	if op <= 1 then
		-- Crop to Img
		outattrs = {IMG_Like = inimg, IMG_NoData = req:IsPreCalc(),}
		if op == 0  then
			outattrs.IMG_DataWindow = inimg.ImageWindow
		end
	elseif op >= 2 then
		-- Expand to STMap
		outattrs = {IMG_Like = stMapimg, IMG_NoData = req:IsPreCalc(),}
	end

	return Image(outattrs)
end


function PreCalcProcess(req)
	local out = ClacDod(req)

	OutImage:Set(req, out)
end


function Process(req)
	local inimg = InImage:GetValue(req)
	local stMapimg = InSTMapImage:GetValue(req)

	local out = ClacDod(req)

	if not req:IsPreCalc() then

		local node = DVIPComputeNode(req, "STMapPKernel", Source, "STMapPParams", STMapPParams)

		local params = node:GetParamBlock(STMapPParams)

		-- The complete size of the image, including DoD
		params.imgSize[0] = inimg.Width
		params.imgSize[1] = inimg.Height

		params.imgDoD[0] = inimg.DataWindow:Width()
		params.imgDoD[1] = inimg.DataWindow:Height()

		-- The offset from the DoD's corner to the images corner
		params.imgOffset[0] = inimg.DataWindow.left
		params.imgOffset[1] = inimg.DataWindow.bottom
		params.imgOffset[2] = inimg.Width - inimg.DataWindow.right
		params.imgOffset[3] = inimg.Height - inimg.DataWindow.top

		-- Get the difference between the STMap and the texture
		local op =  math.floor(InOperation:GetValue(req).Value + 0.5)
		if op == 0 then
			params.stMapOffset[0] = ((inimg.Width - stMapimg.Width) * 0.5) + stMapimg.DataWindow.left
			params.stMapOffset[1] = ((inimg.Height - stMapimg.Height) * 0.5) + stMapimg.DataWindow.bottom
		elseif op == 1 then
			-- crop to image Size
			params.stMapOffset[0] = ((inimg.Width - stMapimg.Width) * 0.5) - inimg.DataWindow.left
			params.stMapOffset[1] = ((inimg.Height - stMapimg.Height) * 0.5) - inimg.DataWindow.bottom
		elseif op == 2 then
			-- Expand to STMap
			params.stMapOffset[0] = 0
			params.stMapOffset[1] = 0
		end


		local matchRender = InMatchRender:GetValue(req).Value
		params.pixelScale[0] = 1
		params.pixelScale[1] = 1
		if matchRender == 0 then
			matchRender = 0
			params.pixelScale[0] = 1 - (1/inimg.Width)
			params.pixelScale[1] = 1 - (1/inimg.Height)
		elseif matchRender == 1 then
			matchRender = 0
		elseif matchRender == 2 then
			matchRender = 0.5
		end

		params.hiQ = not req:IsQuick()
		params.matchRender = matchRender
		params.uChannel = InUChannel:GetValue(req).Value
		params.vChannel = InVChannel:GetValue(req).Value
		params.flipU = InFlipV:GetValue(req).Value
		params.flipV = InFlipU:GetValue(req).Value
		params.offset[0] = InOffset:GetValue(req).X
		params.offset[1] = InOffset:GetValue(req).Y
		params.repeatPivot[0] = InRepeatPivot:GetValue(req).X - 0.5
		params.repeatPivot[1] = InRepeatPivot:GetValue(req).Y - 0.5
		params.tile = InTiling:GetValue(req).Value
		params.hRepeat = InHRepeat:GetValue(req).Value + InCombRepeat:GetValue(req).Value - 1
		params.vRepeat = InVRepeat:GetValue(req).Value + InCombRepeat:GetValue(req).Value - 1
		params.preDivideSTMap = InPreDivideSTMap:GetValue(req).Value
		params.postMultiplyRGB = InPostMultiplyRGB:GetValue(req).Value

		params.compOrder = out:IsMask() and 1 or 15
		
		node:SetParamBlock(params)

		node:AddInput("inimg", inimg)
		node:AddInput("stMapimg", stMapimg)
		node:AddOutput("outimg", out)
		
		local ok = node:RunSession(req)
		if not ok then
			out = inimg
			dump(node:GetErrorLog())
		end
	end

	OutImage:Set(req, out)
	collectgarbage()
end

STMapPParams = [[
	int imgSize[2];
	int imgDoD[2];
	float imgOffset[4];
	int stMapOffset[2];
	bool hiQ;

	float matchRender;
	float pixelScale[2];

	int uChannel;
	int vChannel;
	bool flipU;
	bool flipV;
	float offset[2];
	float repeatPivot[2];
	int tile;
	float hRepeat;
	float vRepeat;
	bool preDivideSTMap;
	bool postMultiplyRGB;

    int compOrder;
]]

Source = [[

__DEVICE__ float stMaptile( float stMap, int imgDoD, float imgOffset)
{
	stMap = int(stMap) % imgDoD;
	if (stMap < 0) {
		stMap = stMap + imgDoD;
	}
	return stMap;
}

__DEVICE__ float stMapmirror( float stMap, int imgDoD, float imgOffset)
{
	int mirrored = (int(stMap) / imgDoD) % 2;
	int newSTMap = int(stMap) % imgDoD;
	if (newSTMap < 0) {
		newSTMap = newSTMap + imgDoD;
		mirrored = !mirrored;
	}

	if (mirrored) {
		newSTMap = imgDoD - newSTMap;
	}
	


	return newSTMap;
}

__DEVICE__ float4 getPixel( __TEXTURE2D__ inimg, float stMapx, float stMapy, int compOrder, int tile, int2 imgDoD, float4 imgOffset)
{
	// STMap Tiling (Transparent happen later on, Extend is default behaviour)
	if (tile == 2) //Tile
	{
		stMapx = stMaptile(stMapx, imgDoD.x, imgOffset.x);
		stMapy = stMaptile(stMapy, imgDoD.y, imgOffset.y);
	}
	else if (tile == 3) //Mirror
	{
		stMapx = stMapmirror(stMapx, imgDoD.x, imgOffset.x);
		stMapy = stMapmirror(stMapy, imgDoD.y, imgOffset.y);
	}
	else if (tile == 0) //Transparent
	{
		if (stMapx > imgDoD.x || stMapy > imgDoD.y || stMapx < 0 || stMapy < 0)
		{
			return to_float4(0,0,0,0);
		}
	}

	return _tex2DVecN(inimg, stMapx, stMapy, compOrder);
}

__KERNEL__ void STMapPKernel(
	__CONSTANTREF__ STMapPParams *params,
	__TEXTURE2D__ inimg,
	__TEXTURE2D__ stMapimg,
	__TEXTURE2D_WRITE__ outimg
){
	
	DEFINE_KERNEL_ITERATORS_XY(x, y);
	float4 fragColor;

	float2 iResolution = to_float2(params->imgSize[0], params->imgSize[1]);
	float4 imgOffset = to_float4(params->imgOffset[0], params->imgOffset[1], params->imgOffset[2], params->imgOffset[3]);

	int2 stMapOffset = to_int2(params->stMapOffset[0], params->stMapOffset[1]);

	float4 stMappos = _tex2DVecN(stMapimg, x - stMapOffset.x, y - stMapOffset.y, params->compOrder);

	float2 stMap;
	// CHANNEL SELECT
	if(params->uChannel == 0) // Red
	{
		stMap.x = stMappos.x;
	}
	else if(params->uChannel == 1) // Green
	{
		stMap.x = stMappos.y;
	}
	else if(params->uChannel == 2) // Blue
	{
		stMap.x = stMappos.z;
	}

	if(params->vChannel == 0) // Red
	{
		stMap.y = stMappos.x;
	}
	else if(params->vChannel == 1) // Green
	{
		stMap.y = stMappos.y;
	}
	else if(params->vChannel == 2) // Blue
	{
		stMap.y = stMappos.z;
	}

	// FLIP
	if (params->flipU)
	{
		stMap.x = 1.0f - stMap.x;
	}
	if (params->flipV)
	{
		stMap.y = 1.0f - stMap.y;
	}

	//Apply STMap Modifications
	stMap.x *= params->pixelScale[0];
	stMap.y *= params->pixelScale[1];

	stMap -= to_float2(params->repeatPivot[0], params->repeatPivot[1]);
	stMap.x *= params->hRepeat;
	stMap.y *= params->vRepeat;
	stMap += to_float2(params->repeatPivot[0], params->repeatPivot[1]);

	stMap -= to_float2(params->offset[0] * params->hRepeat, params->offset[1] * params->vRepeat);
	// Upscale STMap to pixel
	stMap *= iResolution;

	stMap -= to_float2(imgOffset.x, imgOffset.y);
	// Apply shift to STmap depending on Match Render setting
	stMap -= to_float2_s(params->matchRender);

	// If pre-divide STMap
	if (params->preDivideSTMap && stMappos.w != 0.0)
	{
		stMap /= stMappos.w;
	}

	// Move the STmap to the center
	stMap += iResolution*0.5f;
	int2 imgDoD = to_int2(params->imgDoD[0], params->imgDoD[1]);
		
	if (params->hiQ)
	{
		// If subpixel
		bool subX = false;
		bool subY = false;
		
		if (fmod(stMap.x, 1) != 0)
		{
			subX = true;
		}
		if (fmod(stMap.y, 1) != 0)
		{
			subY = true;
		}

		if (subX && subY)
		{
			float4 pix1 = getPixel(inimg, _floor(stMap.x), _floor(stMap.y), params->compOrder, params->tile, imgDoD, imgOffset);
			float4 pix2 = getPixel(inimg, _ceil(stMap.x), _floor(stMap.y), params->compOrder, params->tile, imgDoD, imgOffset);
			float4 pix3 = getPixel(inimg, _floor(stMap.x), _ceil(stMap.y), params->compOrder, params->tile, imgDoD, imgOffset);
			float4 pix4 = getPixel(inimg, _ceil(stMap.x), _ceil(stMap.y), params->compOrder, params->tile, imgDoD, imgOffset);

			fragColor = _mix(
							_mix(pix1, pix2, stMap.x - _floor(stMap.x)),
							_mix(pix3, pix4, stMap.x - _floor(stMap.x)),
							stMap.y - _floor(stMap.y)
							);
		}
		else if (subX)
		{
			float4 pix1 = getPixel(inimg, _floor(stMap.x), stMap.y, params->compOrder, params->tile, imgDoD, imgOffset);
			float4 pix2 = getPixel(inimg, _ceil(stMap.x), stMap.y, params->compOrder, params->tile, imgDoD, imgOffset);

			fragColor = _mix(pix1, pix2, stMap.x - _floor(stMap.x));
		}
		else if (subY)
		{
			float4 pix1 = getPixel(inimg, stMap.x, _floor(stMap.y), params->compOrder, params->tile, imgDoD, imgOffset);
			float4 pix2 = getPixel(inimg, stMap.x, _ceil(stMap.y), params->compOrder, params->tile, imgDoD, imgOffset);

			fragColor = _mix(pix1, pix2, stMap.y - _floor(stMap.y));
		}
		else
		{
			fragColor = getPixel(inimg, stMap.x, stMap.y, params->compOrder, params->tile, imgDoD, imgOffset);
		}
	}
	else
	{
		fragColor = getPixel(inimg, stMap.x, stMap.y, params->compOrder, params->tile, imgDoD, imgOffset);
	}


	// Post-Multiply RGB
	if (params->postMultiplyRGB)
	{	
		fragColor.x *= stMappos.w;
		fragColor.y *= stMappos.w;
		fragColor.z *= stMappos.w;
		fragColor.w *= stMappos.w;
	}

	_tex2DVec4Write(outimg, x, y, fragColor);
	}
]]