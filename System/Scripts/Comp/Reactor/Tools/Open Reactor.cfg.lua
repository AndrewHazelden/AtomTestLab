-- Open Reactor.cfg menu item

-- Get the current OS platform
platform = (FuPLATFORM_WINDOWS and 'Windows') or (FuPLATFORM_MAC and 'Mac') or (FuPLATFORM_LINUX and 'Linux')

function OpenDocument(title, appPath, docPath)
	if platform == 'Windows' then
		-- Running on Windows
		command = 'start "" "' .. appPath .. '" "' .. docPath .. '" &'
	elseif platform == 'Mac' then
		-- Running on Mac
		command = 'open -a "' .. appPath .. '" "' .. docPath .. '" &'
	 elseif platform == "Linux" then
		-- Running on Linux
		command = '"' .. appPath .. '" "' .. docPath .. '" &'
	else
		print('[Error] There is an invalid Fusion platform detected')
		return
	end

	print('[' .. title .. '] [App] "' .. appPath .. '" [Document] "' .. docPath .. '"')
	print('[Launch Command] ' .. tostring(command))
	os.execute(command)
end

-- The 'Reactor:/System/Reactor.cfg' holds the preferences and the repository list:
local reactor_cfg = app:MapPath('Reactor:/System/Reactor.cfg')

-- Check if a script editor has been selected in the Fusion "Script" preferences section.
editorPath = fu:GetPrefs('Global.Script.EditorPath')
if editorPath == nil or editorPath == "" then
	print('[Open Reactor.cfg in Script Editor] The "Editor Path" is empty. Please choose a text editor in the Fusion Preferences "Global and Default Settings > Script > Editor Path" section.')
	app:ShowPrefs('PrefsScript')
	os.exit()
end

-- Check if the 'Reactor.cfg' file exists on disk
if bmd.fileexists(reactor_cfg) == false then
	print('[Reactor.cfg] File Missing: "' .. tostring(reactor_cfg) ..  [[". You need to add a 'Reactor.cfg' to the "Reactor:/System/" PathMap folder to fix this error.]])
	os.exit()
else
	OpenDocument('Open Reactor.cfg in Script Editor', editorPath, app:MapPath(reactor_cfg))
	print('[Reactor CFG] ' .. reactor_cfg .. '\n')
end
